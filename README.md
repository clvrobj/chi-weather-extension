Chi weather extension
=====================

Show the current weather in your browser.

Features
--------
* Show the current weather info(weather summary, temperature and Skycon animated icon) in popup window
* Show the current weather on toolbar icon

Development
-----------
Install dependencies:

```bash
$ npm install
$ bower install
```

LiveReload:

```bash
$ gulp watch
```

Build the extension:

```bash
$ gulp build
```

For testing, please open the `chrome-extension://[Extension ID]/tests/tests.html` in the browser to check the test results.
