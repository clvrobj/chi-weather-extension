'use strict';

var lastWeatherResp = null;

var getCurrentWeather = function (callback) {
  var getWeather = function (position) {
	var latitude = position.coords.latitude;
	var longitude = position.coords.longitude;
    Weather.get(latitude, longitude, function (data) {
      chrome.browserAction.setIcon({path: 'images/' + data.currently.icon + '.png'});
      lastWeatherResp = data;
      if(typeof callback === 'function'){
        callback(data);
      }
    });
  };

  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(getWeather);
  }
};

chrome.runtime.onInstalled.addListener(function() {
  chrome.alarms.create('forecast', {
    delayInMinutes: 0,
    periodInMinutes: 8
  });
});

chrome.alarms.onAlarm.addListener(function( alarm ) {
  getCurrentWeather();
});

chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
  if (request.action == 'getCurrentWeather') {
    if (lastWeatherResp) {
      sendResponse(lastWeatherResp);
    } else {
      getCurrentWeather(function(data) {
        console.log('getCurrentWeather done');
        var views = chrome.extension.getViews({type: 'popup'});
        if (views && views.length > 0) {
          var popup = views[0];
          popup && popup.$rootScope &&
            popup.$rootScope.$broadcast('show-weather', data);
        }
      });
    }
  }
});
