var app = angular.module('weatherApp', ['angular-skycons']);

app.controller(
  'WeatherCtrl',
  ['$rootScope', '$scope', '$window',
   function($rootScope, $scope, $window) {
     $window.$rootScope = $rootScope;

     var showWeather = function (response) {
       $scope.isLoading = false;
       $scope.summary = response.currently.summary;
       $scope.temperature = response.currently.temperature.toString().split('.')[0] + '˚C';
       $scope.CurrentWeather = {
         forecast: {
           icon: response.currently.icon,
           iconSize: 100,
           color: 'blue'
         }
       };
       $scope.$apply();
     };

     $scope.$on('show-weather', function (e, response) {
       showWeather(response);
     });

     $scope.init = function () {
       $scope.isLoading = true;
       chrome.runtime.sendMessage({action: 'getCurrentWeather'}, function(response) {
         if (response) {
	       showWeather(response);
         }
       });
     };
   }
  ]
);
