var Weather = {
  apiKey: '2f107ddf8be528e236da239a1d3ec1ae',
  get: function (latitude, longitude, options, callback) {
    if (typeof options === 'function') {
      callback = options;
      options = {};
    }
    options.units = 'si';
    var url = 'https://api.darksky.net/forecast/' + this.apiKey + '/';
    url = url + latitude + ',' + longitude;
    $.ajax({
      url: url,
      type : 'GET',
      data: options,
      dataType: 'json',
      crossDomain: true
    }).done(function (data) {
      callback(data);
    });
  }
};
