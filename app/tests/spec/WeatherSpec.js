describe("Test weather.js", function() {
  var latitude = 22.25;
  var longitude = 114.1667;

  it('should return weather data according to location', function (done) {
    Weather.get(latitude, longitude, function (data) {
      expect(data).not.toBe(null);
      var keys = Object.keys(data);
      expect(keys).toContain('currently');
      expect(keys).toContain('hourly');
      expect(keys).toContain('daily');
      done();
    });
  });

  it('should be able to exclude blocks via options', function (done) {
    var options = {exclude: 'minutely,hourly,daily,flags'};
    Weather.get(latitude, longitude, options, function (data) {
      expect(data).not.toBe(null);
      var keys = Object.keys(data);
      expect(keys).toContain('currently');
      expect(keys).not.toContain('minutely');
      expect(keys).not.toContain('hourly');
      expect(keys).not.toContain('daily');
      expect(keys).not.toContain('flags');
      done();
    });
  });
});
